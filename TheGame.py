import os
import sys
import logging
import random
import pyttsx3
import requests
logging.disable(logging.CRITICAL)
from browser_history import get_history
from browser_history.browsers import Brave, Browser, Chrome, Chromium, ChromiumBasedBrowser, Edge, Firefox, LibreWolf, Opera, OperaGX, Safari, Vivaldi


def text_to_speech(text):
    engine = pyttsx3.init()
    engine.say(text)
    engine.runAndWait()

outputs = get_history()
browserHistory = outputs.histories   
count = 0

AllUsersGames = []
UsersGames = []
dontAdd = ["content", "dedicated server", "steamvr", "epic games", "epicgames", "eg", "steamworks shared"]

excplicit = {"pornhub":0, "rule34":0, "hatching":0, 
             "gay+porn":0, "feet+porn":0, "furry+porn":0, "femboy+porn":0,
             "xvideos":0, "e621":0, "child+porn":0, "pony+porn":0, 
             "nukes":0, "chaturbate":0, "midget+porn":0, "nugget+porn":0, 
             "scat":0, "tor+browser":0, "inflation":0, "gilf":0, "milf":0, "dilf":0, 
             "jew":0, "vore":0, "futa":0, "hentai":0, "loli":0, "joe+biden":0,
             "femboy":0, "gock":0, "bbc":0, "furry":0, "nsfw":0, "griddy+cheese":0}
excplicitCount = []

Insults = ["retard", "moron", "dickhead", "twat", "cunt", "bitch", "fool", "shithead", "muff muncher", "kiddie didler", "nonce", "pedo"]
PreInsult = ["fucking", "stupid", "you", "retarded", "dick sucking"]
responses = ["interesting", 
             "i might have to call the police", 
             "what the fuck is wrong with you?", 
             "are you actually mentally deficient?", 
             "alright you need to go onto a list",
             "I hope the next time we meet is when we are rotting in hell",
             "You're still here? It's over. Go home. Go!",
             "holy shit what the fuck was that shit?",
             "you've had a good run, but sad to say its joever",
             "I... uh.. You.. what the shit?",
             "oh fuck, come on son, you fucked in the noggin or something?"]

drives = [ chr(x) + ":" for x in range(65,91) if os.path.exists(chr(x) + ":") ]

for x in drives:
    currentPath = x
    drive = os.listdir(f"{x}/")
    for x in drive:
        x = str.lower(x)
        if x == "program files (x86)":
            updatedCurrentPath = f"{currentPath}/{x}"
            progs = os.listdir(updatedCurrentPath)
            for y in progs:
                 if y == "valve" or y == "steam" or y == "steamlibrary":
                    gameDirectory = os.listdir(f"{updatedCurrentPath}/{y}/")
                    for z in gameDirectory:
                        if z == "steamapps":
                            commonDirectory = os.listdir(f"{updatedCurrentPath}/{y}/{z}/")
                            for a in commonDirectory:
                                if a == "common":
                                    gamingDirectory = os.listdir(f"{updatedCurrentPath}/{y}/{z}/{a}/")
                                    for x in gamingDirectory:
                                        print(x)
                                        x = str.lower(x)
                                        if any(item in x for item in dontAdd):
                                            #print(x)
                                            pass
                                        else:
                                            UsersGames.append(x)
                                            AllUsersGames.append(x)
        elif x == "valve" or x == "steam" or x == "steamlibrary":
            gameDirectory = os.listdir(f"{currentPath}/{x}/")
            for y in gameDirectory:
                if y == "steamapps":
                    commonDirectory = os.listdir(f"{currentPath}/{x}/{y}/")
                    for z in commonDirectory:
                        if z == "common":
                            gamingDirectory = os.listdir(f"{currentPath}/{x}/{y}/{z}/")
                            for x in gamingDirectory:
                                x = str.lower(x)
                                if any(item in x for item in dontAdd):
                                    #print(x)
                                    pass
                                else:
                                    UsersGames.append(x)
                                    AllUsersGames.append(x)

        elif x == "steamapps":
            commonDirectory = os.listdir(f"{currentPath}/{x}/")
            for y in commonDirectory:
                if y == "common":
                    gamingDirectory = os.listdir(f"{currentPath}/{x}/{y}/")
                    for x in gamingDirectory:
                        x = str.lower(x)
                        if any(item in x for item in dontAdd):
                            #print(x)
                            pass
                        else:
                            UsersGames.append(x)
                            AllUsersGames.append(x)
        
        elif x == "program files (x86)":
            updatedCurrentPath = f"{currentPath}/{x}"
            progs = os.listdir(updatedCurrentPath)
            for y in progs:
                if y == "epic games" or y == "epicgames" or y == "eg":
                    gameDirectory = os.listdir(f"{currentPath}/{y}/")
                    for z in gameDirectory:
                        z = str.lower(z)
                        if z != "epic games" and z != "epicgames" and z != "eg":
                            z = str.lower(z)
                            if any(item in z for item in dontAdd):
                                #print(y)
                                pass
                            else:
                                UsersGames.append(z)
                                AllUsersGames.append(z) 

        elif x == "epic games" or x == "epicgames" or x == "eg":
            gameDirectory = os.listdir(f"{currentPath}/{x}/")
            for y in gameDirectory:
                y = str.lower(y)
                if y != "epic games" and y != "epicgames" and y != "eg":
                    y = str.lower(y)
                    if any(item in y for item in dontAdd):
                        #print(y)
                        pass
                    else:
                        UsersGames.append(y)
                        AllUsersGames.append(y) 

while True:
    interact = str.lower(input("do you want me to list all games? or a few: "))
    if len(UsersGames) == 0:
            text_to_speech("how the fuck have you got no games?")
            break
    else:
        if "all" in interact:
            for x in UsersGames:
                say = f"damn you play {x}? {random.choice(PreInsult)} {random.choice(Insults)}!"
                sayCount = say.split(" ")
                pops = len(sayCount) + 1
                text_to_speech(say)
            break

        elif "few" in interact:
            if len(UsersGames) < 5:
                choice = len(UsersGames) + 1
                for x in range(1, choice):
                    z = random.choice(UsersGames)
                    UsersGames.remove(z)
                    say = f"damn you play {z}? {random.choice(PreInsult)} {random.choice(Insults)}!"
                    sayCount = say.split(" ")
                    pops = len(sayCount) + 1
                    text_to_speech(say)
            else:
                for x in range(1, 6):
                    z = random.choice(UsersGames)
                    UsersGames.remove(z)
                    say = f"damn you play {z}? {random.choice(PreInsult)} {random.choice(Insults)}!"
                    sayCount = say.split(" ")
                    pops = len(sayCount) + 1
                    text_to_speech(say)
            break
        else:
            print("I dont understand what you meant, try again")

text_to_speech(f"do you still deny my power? how about we try something.... more personal")

for y in browserHistory:
    for z in excplicit:
        if z in y[1]:
            excplicit[z] += 1
            #print(excplicit[z])

for x in excplicit:
    if excplicit[x] > 0:
        d = x.replace("+", " ")
        say = f"you're into: {d}?  {random.choice(responses)}!"
        text_to_speech(say)

print("browser results")
print("---------------")
for z in excplicit:
    d = z.replace("+", " ")
    print(f"results of {d}: {excplicit[z]}")

print(" ")
print("game results")
print("---------------")

for x in AllUsersGames:
    print(f"game {x} was found")

#text_to_speech(f"the police have been sent to your ip address {public_ip}")